# Simple MQTT client written in go.
I wrote this so I could have a statically linked bin to copy around and with some extra features.

 - Parse Golang templates
 - Exec script on incomming message


## Usage

    A simple MQTT client

    Usage:
      mqtt [command]

    Available Commands:
      pub         A simple MQTT version 3.1 client that will publish a single message on a topic
      sub          is a simple MQTT client that will subscribe to a topic and print the messages that it receives.

    Flags:
          --cafile string     Path to a file containing PEM encoded CA certificates that are trusted
      -i, --clientid string   A client id for the connection (default "ipa59")
          --config string     Config file (default is $HOME/.mqttc.yaml)
          --debug             Enable debug messages
          --keepalive int     Time in seconds between sending keepalive requests (default 30)
          --password string   Provide a password to be used for authenticating with the broker
          --qos int           Specify the quality of service to use for the message, from 0, 1 and 2
          --quiet             Be quiet about things
          --reconint int      Reconnection interval in seconds (default 1)
      -S, --server string     The full URL of the MQTT server to connect to (default "ssl://127.0.0.1:8883")
          --username string   Provide a username to be used for authenticating with the broker.

    Use "mqtt [command] --help" for more information about a command.


## Subscribe

    Output formats:
            plain: "message"
            topic: "topic":"message"
            tmpl: parse a go template (need --template/-T option)

            Template variables:
                {{ .topic }} and {{ .message }}

    Usage:
      mqtt sub [flags]

    Flags:
      -e, --exec string       Run executable with topic as first argument and message as second
      -f, --format string     Output format any of, plain, topic, tmpl (default "plain")
      -T, --template string   When using format: tmpl, specify path to template to parse
      -t, --topic string      The MQTT topic on which to subscribe to

    Global Flags:
          --cafile string     Path to a file containing PEM encoded CA certificates that are trusted
      -i, --clientid string   A client id for the connection (default "ipa19")
          --config string     Config file (default is $HOME/.mqttc.yaml)
          --debug             Enable debug messages
          --password string   Provide a password to be used for authenticating with the broker
          --qos int           Specify the quality of service to use for the message, from 0, 1 and 2
          --quiet             Be quiet about things
      -S, --server string     The full URL of the MQTT server to connect to (default "ssl://127.0.0.1:8883")
          --username string   Provide a username to be used for authenticating with the broker.


### Template example
    "{{ .topic }}";"{{ .message }}"

### Exec example
    #!/bin/bash
    echo "Topic is $1 with payload: $2"


## Publish

    A simple MQTT version 3.1 client that will publish a single message on a topic

    Usage:
      mqtt pub [flags]

    Flags:
      -m, --message string   The message to send to the topic
      -r, --retain           Message sent out will be treated as a retained message
      -t, --topic string     The MQTT topic on which to publish the message

    Global Flags:
          --cafile string     Path to a file containing PEM encoded CA certificates that are trusted
      -i, --clientid string   A client id for the connection (default "ipa58")
          --config string     Config file (default is $HOME/.mqttc.yaml)
          --debug             Enable debug messages
          --password string   Provide a password to be used for authenticating with the broker
          --qos int           Specify the quality of service to use for the message, from 0, 1 and 2
          --quiet             Be quiet about things
      -S, --server string     The full URL of the MQTT server to connect to (default "ssl://127.0.0.1:8883")
          --username string   Provide a username to be used for authenticating with the broker.
