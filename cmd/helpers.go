package cmd

import (
	"fmt"

	"github.com/spf13/viper"
)

// Ptf is a convenience wrapper for fmt.Printf.
//
// Calling Ptf(f, x, y) is equivalent to
// fmt.Printf(f, Formatter(x), Formatter(y)).
func Ptf(format string, a ...interface{}) (n int, errno error) {
	if viper.GetBool("quiet") {
		return 0, nil
	}
	return fmt.Printf(format, a...)
}

// Ptn is a convenience wrapper for fmt.Printf.
//
// Calling Ptn(f, x, y) is equivalent to
// fmt.Println(f, x, y).
func Ptn(a ...interface{}) (n int, errno error) {
	if viper.GetBool("quiet") {
		return 0, nil
	}
	return fmt.Println(a...)
}
