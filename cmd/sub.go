// Copyright © 2016 NAME Fredrik <fredrik@ppo2.se>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"text/template"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// subCmd represents the sub command
var subCmd = &cobra.Command{
	Use:   "sub",
	Short: " is a simple MQTT client that will subscribe to a topic and print the messages that it receives.",
	Long: `Output formats:
		plain: "message"
		topic: "topic":"message"
		tmpl: parse a go template (need --template/-T option)

		Template variables:
			{{ .topic }} and {{ .message }}`,
	Run: func(cmd *cobra.Command, args []string) {
		sub(cmd)
	},
}

func init() {
	RootCmd.AddCommand(subCmd)
	subCmd.Flags().StringP("topic", "t", "", "The MQTT topic on which to subscribe to")
	subCmd.Flags().StringP("format", "f", "plain", "Output format any of, plain, topic, tmpl")
	subCmd.Flags().StringP("template", "T", "", "When using format: tmpl, specify path to template to parse")
	subCmd.Flags().StringP("exec", "e", "", "Run executable with topic as first argument and message as second")
	viper.BindPFlag("subtopic", subCmd.Flags().Lookup("topic"))
	viper.BindPFlag("format", subCmd.Flags().Lookup("format"))
	viper.BindPFlag("template", subCmd.Flags().Lookup("template"))
	viper.BindPFlag("exec", subCmd.Flags().Lookup("exec"))
}

func sub(cmd *cobra.Command) {

	if viper.GetBool("Debug") {
		MQTT.DEBUG = log.New(os.Stdout, "", 0)
		MQTT.ERROR = log.New(os.Stdout, "", 0)
		MQTT.WARN = log.New(os.Stdout, "", 0)
	}

	q := make(chan bool, 1)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		q <- true
		fmt.Println("signal received, exiting")
		os.Exit(0)
	}()

	connOpts := &MQTT.ClientOptions{
		ClientID:             viper.GetString("clientid"),
		CleanSession:         true,
		MaxReconnectInterval: time.Duration(viper.GetInt("reconint")) * time.Second,
		KeepAlive:            time.Duration(viper.GetInt("keepalive")) * time.Second,
	}

	tlsConfig := &tls.Config{}

	if viper.GetString("cafile") != "" {
		certpool := x509.NewCertPool()
		pemCerts, err := ioutil.ReadFile(viper.GetString("cafile"))
		if err == nil {
			certpool.AppendCertsFromPEM(pemCerts)
		}

		tlsConfig = &tls.Config{
			InsecureSkipVerify: true,
			ClientAuth:         tls.NoClientCert,
			RootCAs:            certpool,
			ClientCAs:          nil,
		}
		connOpts.SetTLSConfig(tlsConfig)
	}

	if viper.GetString("username") != "" {
		connOpts.SetUsername(viper.GetString("username"))
		if viper.GetString("password") != "" {
			connOpts.SetPassword(viper.GetString("password"))
		}
	}
	connOpts.AddBroker(viper.GetString("server"))

	lflagformat := viper.GetString("format")
	lflagtemplate := viper.GetString("template")
	if lflagformat == "tmpl" && lflagtemplate == "" {
		fmt.Println("Error: Need --template file to parse")
		cmd.Usage()
		return
	}

	var onMes func(*MQTT.Client, MQTT.Message)

	if viper.GetString("exec") != "" {
		Ptn("Exec mode active")
		onMes = func(client *MQTT.Client, message MQTT.Message) {
			subExecutor(client, message)
		}
	} else {
		onMes = func(client *MQTT.Client, message MQTT.Message) {
			messageWriter(client, message, lflagformat, lflagtemplate)
		}
	}

	connOpts.OnConnect = func(c *MQTT.Client) {
		if token := c.Subscribe(viper.GetString("subtopic"), byte(viper.GetInt("qos")), onMes); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	client := MQTT.NewClient(connOpts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		Ptn(token.Error())
		os.Exit(255)
	}
	defer client.Disconnect(1000)

	// Block untill end of time... or ctr-c.
	<-q

}

func subExecutor(client *MQTT.Client, message MQTT.Message) {
	Ptf("Exec: %s with topic %s\n", viper.GetString("exec"), message.Topic())
	cmd := exec.Command(viper.GetString("exec"), message.Topic(), string(message.Payload()))
	output, err := cmd.Output()
	if err != nil {
		Ptn(err)
		return
	}

	if string(output) == "" {
		return
	}
	Ptn(string(output))
}

// TODO: use a Writer
func messageWriter(client *MQTT.Client, message MQTT.Message, format string, templatefile string) {
	switch format {
	case "plain":
		fmt.Printf("%s\n", message.Payload())
	case "topic":
		fmt.Printf("%s : %s\n", message.Topic(), message.Payload())
	case "tmpl":
		templateWriter(message, templatefile)
	default:
		fmt.Printf("%s\n", message.Payload())
	}
}

// TODO: use a Writer
func templateWriter(message MQTT.Message, templatefile string) {
	data := make(map[string]string)
	data["topic"] = message.Topic()
	data["message"] = string(message.Payload())
	t := template.Must(template.ParseFiles(templatefile))
	err := t.Execute(os.Stdout, data)
	if err != nil {
		panic(err)
	}
}
