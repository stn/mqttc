// Copyright © 2016 stone <fredrik@ppo2.se>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bufio"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"log"
	"os"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// pubCmd represents the pub command
var pubCmd = &cobra.Command{
	Use:   "pub",
	Short: "A simple MQTT version 3.1 client that will publish a single message on a topic",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		pub()
	},
}

func init() {
	RootCmd.AddCommand(pubCmd)

	pubCmd.Flags().StringP("topic", "t", "", "The MQTT topic on which to publish the message")
	pubCmd.Flags().StringP("message", "m", "", "The message to send to the topic")
	pubCmd.Flags().BoolP("retain", "r", false, "Message sent out will be treated as a retained message")
	viper.BindPFlag("pubtopic", pubCmd.LocalFlags().Lookup("topic"))
	viper.BindPFlag("message", pubCmd.Flags().Lookup("message"))
	viper.BindPFlag("retain", pubCmd.Flags().Lookup("retain"))
}

func pub() {

	if viper.GetBool("debug") {
		MQTT.DEBUG = log.New(os.Stdout, "", 0)
		MQTT.ERROR = log.New(os.Stdout, "", 0)
		MQTT.WARN = log.New(os.Stdout, "", 0)
	}

	connOpts := &MQTT.ClientOptions{
		ClientID:             viper.GetString("clientid"),
		CleanSession:         true,
		MaxReconnectInterval: time.Duration(viper.GetInt("reconint")) * time.Second,
		KeepAlive:            time.Duration(viper.GetInt("keepalive")) * time.Second,
	}

	tlsConfig := &tls.Config{}

	if viper.GetString("cafile") != "" {

		certpool := x509.NewCertPool()
		pemCerts, err := ioutil.ReadFile(viper.GetString("cafile"))
		if err == nil {
			certpool.AppendCertsFromPEM(pemCerts)
		}

		tlsConfig = &tls.Config{
			InsecureSkipVerify: true,
			ClientAuth:         tls.NoClientCert,
			RootCAs:            certpool,
			ClientCAs:          nil,
		}
		connOpts.SetTLSConfig(tlsConfig)
	}

	if viper.Get("username") != "" {
		connOpts.SetUsername(viper.GetString("username"))
		if viper.Get("password") != "" {
			connOpts.SetPassword(viper.GetString("password"))
		}
	}
	connOpts.AddBroker(viper.GetString("server"))

	client := MQTT.NewClient(connOpts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		Ptn(token.Error())
		os.Exit(255)
	}
	defer client.Disconnect(1000)

	var sendMessage string
	if viper.GetString("message") == "" {
		Ptn("Reading from stdin...")
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			sendMessage += scanner.Text()
		}
	} else {
		sendMessage = viper.GetString("message")
	}

	Ptf("[%s]: %s → %s\n", viper.GetString("server"), viper.GetString("pubtopic"), sendMessage)
	token := client.Publish(viper.GetString("pubtopic"), byte(viper.GetInt("qos")), viper.GetBool("retain"), sendMessage)
	if token.Wait() && token.Error() != nil {
		Ptn(token.Error())
	}

}
