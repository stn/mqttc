// Copyright © 2016 Fredrik <fredrik@ppo2.se>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Global flags
var (
	flagCfgfile string
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "mqtt",
	Short: "A simple MQTT client",
	Long:  ``,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	hostname, _ := os.Hostname()
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&flagCfgfile, "config", "", "Config file (default is $HOME/.mqttc.yaml)")
	RootCmd.PersistentFlags().StringP("server", "S", "ssl://127.0.0.1:8883", "The full URL of the MQTT server to connect to")
	RootCmd.PersistentFlags().StringP("clientid", "i", hostname+strconv.Itoa(time.Now().Second()), "A client id for the connection")
	RootCmd.PersistentFlags().String("cafile", "", "Path to a file containing PEM encoded CA certificates that are trusted")
	RootCmd.PersistentFlags().String("username", "", "Provide a username to be used for authenticating with the broker.")
	RootCmd.PersistentFlags().String("password", "", "Provide a password to be used for authenticating with the broker")
	RootCmd.PersistentFlags().Int("qos", 0, "Specify the quality of service to use for the message, from 0, 1 and 2")
	RootCmd.PersistentFlags().Bool("debug", false, "Enable debug messages")
	RootCmd.PersistentFlags().Bool("quiet", false, "Be quiet about things")
	RootCmd.PersistentFlags().Int("reconint", 1, "Reconnection interval in seconds")
	RootCmd.PersistentFlags().Int("keepalive", 30, "Time in seconds between sending keepalive requests")

	viper.BindPFlag("server", RootCmd.PersistentFlags().Lookup("server"))
	viper.BindPFlag("clientid", RootCmd.PersistentFlags().Lookup("clientid"))
	viper.BindPFlag("cafile", RootCmd.PersistentFlags().Lookup("cafile"))
	viper.BindPFlag("username", RootCmd.PersistentFlags().Lookup("username"))
	viper.BindPFlag("password", RootCmd.PersistentFlags().Lookup("password"))
	viper.BindPFlag("qos", RootCmd.PersistentFlags().Lookup("qos"))
	viper.BindPFlag("debug", RootCmd.PersistentFlags().Lookup("debug"))
	viper.BindPFlag("quiet", RootCmd.PersistentFlags().Lookup("quiet"))
	viper.BindPFlag("reconint", RootCmd.PersistentFlags().Lookup("reconint"))
	viper.BindPFlag("keepalive", RootCmd.PersistentFlags().Lookup("keepalive"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if flagCfgfile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(flagCfgfile)
	}

	viper.SetConfigName(".mqtt") // name of config file (without extension)
	viper.AddConfigPath("$HOME") // adding home directory as first search path
	viper.AutomaticEnv()         // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
